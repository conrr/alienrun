﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
namespace alienrun
{
    class Tile
    {
        //-----------------------------------
        //ENUM
        //-----------------------------------
        public enum TileType
        {
            IMPASSABLE, // = 0, blocks player movment from any direction
            PLATFORM, // = 1, the players movement will be blocked downward ( can not pass through it down)
            LEATHAL, // = 2, this is the tile that will reset the player on contact
            GOAL,   // = 3, player wins level on contact
        }
        //-----------------------------------
        //DATA
        //-----------------------------------
        private Texture2D sprite;
        private Vector2 postion;
        private TileType type;


        //-----------------------------------
        //BEHAVIOR
        //-----------------------------------
        public Tile(Texture2D newSprite, Vector2 newPosition, TileType newType)
        {
            sprite = newSprite;
            postion = newPosition;
            type = newType;
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, postion, Color.White);
        }
        //-----------------------------------
        public Rectangle GetBounds()
        {
            return new Rectangle((int)postion.X, (int)postion.Y, sprite.Width, sprite.Height);
        }
        //-----------------------------------
        public TileType GetTileType()
        {
            return type;
        }
        //-----------------------------------
    }
}
