﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;

namespace alienrun
{
    class Level
    {
        //-----------------------------------
        //DATA
        //-----------------------------------
        private Tile[,] tiles;

        private Texture2D boxTexture;
        private Texture2D spikeTexture;
        private Texture2D bridgeTexture;
        private Texture2D flagTexture;

        private bool completedLevel = false;

        private const int LEVEL_WIDTH = 100;
        private const int LEVEL_HEIGHT = 100;

        private const int TILE_WIDTH = 70;
        private const int TILE_HEIGHT = 70;

        //-----------------------------------
        //BEHAVIOUR
        //-----------------------------------

        public void LoadContent(ContentManager content)
        {
            //creating a single copy of the tile texture that will be used by all tiles
            boxTexture = content.Load<Texture2D>("tiles/box");
            bridgeTexture = content.Load<Texture2D>("tiles/bridge");
            spikeTexture = content.Load<Texture2D>("tiles/spikes");
            flagTexture = content.Load<Texture2D>("tiles/goal");

            SetupLevel();
        }
        //-----------------------------------
        public void SetupLevel()
        {
            completedLevel = false;

            tiles = new Tile[LEVEL_WIDTH, LEVEL_HEIGHT];

            CreateBox(0, 8);
            CreateBox(1, 8);
            CreateBox(2, 8);
            CreateBox(3, 8);
            CreateBox(4, 8);
            CreateBox(5, 8);
            CreateBox(6, 8);
            CreateBox(7, 8);
            CreateBox(8, 8);
            CreateBox(9, 8);

            //create bridges 

            CreateBridge(8, 6);
            CreateBridge(9, 6);
            CreateBridge(10, 6);
            CreateBridge(11, 6);
            CreateBridge(12, 6);
            CreateBridge(13, 6);

            //creat spikes
            CreateSpikes(9, 8);
            CreateSpikes(10, 8);
            CreateSpikes(11, 8);
            CreateSpikes(12, 8);
            CreateSpikes(13, 8);

            //create flag
            CreateFlag(14, 5);

        }
        public void CreateBox(int tileX, int tileY)
        {
            Vector2 tileposition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(boxTexture, tileposition, Tile.TileType.IMPASSABLE);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateBridge(int tileX, int tileY)
        {
            Vector2 tileposition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(bridgeTexture, tileposition, Tile.TileType.PLATFORM);
            tiles[tileX, tileY] = newTile;
        }
        public void CreateSpikes(int tileX, int tileY)
        {
            Vector2 tileposition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(spikeTexture, tileposition, Tile.TileType.LEATHAL);
            tiles[tileX, tileY] = newTile;
        }
        public void CreateFlag(int tileX, int tileY)
        {
            Vector2 tileposition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(flagTexture, tileposition, Tile.TileType.GOAL);
            tiles[tileX, tileY] = newTile;
        }
        //-----------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < LEVEL_WIDTH; ++x)
            {
                for (int y = 0; y < LEVEL_HEIGHT; ++y)
                {
                    if (tiles[x, y] != null)
                        tiles[x, y].Draw(spriteBatch);
                }
            }
        }
        //-----------------------------------
        public List<Tile> GetTilesInBounds(Rectangle bounds)
        {
            //creatting an empty list to fill with tiles
            List<Tile> tilesInBounds = new List<Tile>();

            //dertermine the tile coordinate range for this rectangle
            int leftTile = (int)Math.Floor((float)bounds.Left / TILE_WIDTH);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / TILE_WIDTH) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / TILE_HEIGHT);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / TILE_HEIGHT) - 1;

            //loop through this range ^^^ and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    if (GetTile(x, y) != null)
                        tilesInBounds.Add(tiles[x, y]);
                }
            }
            return tilesInBounds;
        }
        //-----------------------------------
        public Tile GetTile(int x, int y)
        {
            if (x < 0 || x >= LEVEL_WIDTH || y < 0 || y >= LEVEL_HEIGHT)
                return null;

            //otherwise we are out of bounds
            return tiles[x, y];
        }
        //-----------------------------------
        public void CompleteLevel()
        {
            completedLevel = true;
        }
        //-----------------------------------
        public bool GetCompletedLevel() // isLevelComplete()
        {
            return completedLevel;
        }
        //-----------------------------------
    }
}
