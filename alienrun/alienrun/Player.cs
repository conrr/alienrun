﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework.Input;

namespace alienrun
{
    class Player
    {
        //-----------------------------------
        //DATA
        //-----------------------------------
        private Vector2 positon = Vector2.Zero;
        private Vector2 prevPosition = Vector2.Zero;
        private Vector2 velocity = Vector2.Zero;
        private Texture2D sprite = null;
        private Level ourLevel = null;
        private bool touchingGround = false;
        private float jumpButtonTime = 0f;
        private bool jumpLaunchInProgress = false;


        //constants
        private const float MOVE_SPEED = 300.0f;
        private const float GRAVITY = 3400.0f;
        private const float TERMINAL_VEL = 550.0f;
        private const float JUMP_LAUNCH_VEL = -1500.0f;
        private const float MAX_JUMP_TIME = 0.3f;
        private const float JUMP_CONTROL_POWER = 0.9f;
        

        //-----------------------------------
        //BEHAVIOUR
        //-----------------------------------
        //-----------------------------------
        public Player(Level newLevel)
        {
            ourLevel = newLevel;
        }
        //-----------------------------------

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(sprite, positon, Color.White);
        }
        //-----------------------------------
        public void LoadContent(ContentManager content)
        {
            sprite = content.Load<Texture2D>("player/player-stand");
        }
        //-----------------------------------
        
        public void update(GameTime gameTime)
        {
            //if we alreday completed the level we shouldnt load the level;
            if (ourLevel.GetCompletedLevel() == true)
            {
                return;
            }
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //update our velocity based on our input gravity, ect
            //horizontal velocity is always constant as this is a auto run game
            velocity.X = MOVE_SPEED;
            //applying accelaration due to gravity
            velocity.Y += GRAVITY * deltaTime;
            // clamp our vertical velocity to a terminal range
            velocity.Y = MathHelper.Clamp(velocity.Y, -TERMINAL_VEL, TERMINAL_VEL);

            //checking if the player is jumping then apply that to the velocity
            Input(gameTime);

            //pos2 = pos1 + deltaPos
            //deltapos = velocity * deltaTime
            prevPosition = positon;
            positon += velocity * deltaTime;

            // Check if we are colliding with anything
            CheckTileCollision();
        }
        //-----------------------------------
        private void CheckTileCollision()
        {
            //start of by assuming we are not touching the ground on this frame
            touchingGround = false;


            //use the players nounding box to get a list of tiles that we are colliding with
            Rectangle playerBounds = GetBounds();
            Rectangle PrevplayerBounds = GetPrevBounds();
            //this is how to get a list of the tiles
            List<Tile> collidingTiles = ourLevel.GetTilesInBounds(playerBounds);

            //for each colliding tile, move our selfs out of the tile
            foreach (Tile collidingTile in collidingTiles)
            {
                //determine how far we are overlapping the other tile
                Rectangle tileBounds = collidingTile.GetBounds();
                Vector2 depth = GetCollisionDepth(tileBounds, playerBounds);
                Tile.TileType tileType = collidingTile.GetTileType();

                //this will only happen if the collison has happend
                //the depth will be vector 2.zero if no collision happend
                if (depth != Vector2.Zero)
                {
                    if ( tileType == Tile.TileType.LEATHAL)
                    {
                        //stop everything we have been killed
                        KillPlayer();
                        return;
                    }
                    else if (tileType == Tile.TileType.GOAL)
                    {
                        ourLevel.CompleteLevel();
                        return;
                    }
                    float absDepthX = Math.Abs(depth.X);
                    float absDepthY = Math.Abs(depth.Y);

                    //reslove the collision on the shallow axis, as that is the one 
                    // we are the closest to the edge from so it is easyier 
                    //to get the sprite back out
                    if (absDepthY < absDepthX)
                    {
                        // so we know we are falling if we were not previously overlapping but now we are
                        bool fallingontoTile = playerBounds.Bottom > tileBounds.Top && PrevplayerBounds.Bottom <= tileBounds.Top;

                        if (tileType == Tile.TileType.IMPASSABLE || (tileType == Tile.TileType.PLATFORM && fallingontoTile))
                        {
                            //y is our shallow axis
                            //reslove the collsion from the y axis
                            positon.Y += depth.Y;

                            playerBounds = GetBounds();

                            //only if our feet are below the ground 
                            // should we assume we are touching the ground
                            if (playerBounds.Bottom >= tileBounds.Top)
                            {
                                touchingGround = true;
                            }
                        }
                    }
                    // this only handles the left and right collisions IF the tile is IMPASSABLE
                    else if (tileType == Tile.TileType.IMPASSABLE)
                    {
                        //X is our shallow axis
                        //reslove the collision from the x Axis
                        positon.X += depth.X;
                        //recalculate bounds for the futrue collision checking 
                        playerBounds = GetBounds();
                    }


                }

            }

        }
        //-----------------------------------
        
        //-----------------------------------
        private Rectangle GetBounds()
        {
            return new Rectangle((int)positon.X, (int)positon.Y, sprite.Width, sprite.Height);

        }
        private Rectangle GetPrevBounds()
        {
            return new Rectangle((int)prevPosition.X, (int)prevPosition.Y, sprite.Width, sprite.Height);

        }
        //-----------------------------------
        //-----------------------------------
        private Vector2 GetCollisionDepth(Rectangle tile, Rectangle player)
        {
            //this function is to calculate how far our rectangles are over lapping
            // calculate half of the height and width of both rectangles

            float halfWidthPlayer = player.Width / 2.0f;
            float halfHeightPlayer = player.Height / 2.0f;
            float halfWidthtile = tile.Width / 2.0f;
            float halfHeighttile = tile.Height/ 2.0f;

            //calculate the centers of each rectangle
            Vector2 centrePlayer = new Vector2(player.Left + halfWidthPlayer,
                                                 player.Top + halfHeightPlayer);
            Vector2 centretile = new Vector2(tile.Left + halfWidthtile,
                                                 tile.Top + halfHeighttile);

            //how far away are the centres of each of these recatangles from each other
            float distanceX = centrePlayer.X - centretile.X;
            float distanceY = centrePlayer.Y - centretile.Y;

            //minimum distance these need to be to not collide or intersect
            //if either of the x or y distances is greater than the minimum they are not intersecting
            float minDistanceX = halfWidthPlayer + halfWidthtile;
            float minDistanceY = halfHeightPlayer + halfHeighttile;

            //if we are not interceting at all return (0,0
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
            {
                return Vector2.Zero;
            }

            //calculate and return the intersection depth
            //essentially, how much over the minimum intersection distance are we
            //AKA by how much are they intersecting in that direction
            float depthX = 0;
            float depthY = 0;
            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else depthX = -minDistanceX - distanceX;
            if (distanceY > 0)
                depthY = minDistanceY - distanceY;
            else depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);

        }
        //-----------------------------------
        private void Input(GameTime gametime)
        {
            KeyboardState keystate = Keyboard.GetState();
            // are we allowed to jump?
            //only true if we are touching the ground (starting jump)
            //or if we are holding down the button alreday and
            //havent reached our max jump time
            bool allowedtojump = touchingGround == true ||
                (jumpLaunchInProgress == true && jumpButtonTime <= MAX_JUMP_TIME);
            //check if they player is jumping
            if (keystate.IsKeyDown(Keys.Space) && allowedtojump == true)
            {
                
                jumpLaunchInProgress = true;
                jumpButtonTime += (float)gametime.ElapsedGameTime.TotalSeconds;


                //we are trying to jump
                //too simple
                //velocity.Y = JUMP_LAUNCH_VEL;

                //scale launch velocity based on how long the player
                //has held down the jump button
                //we should have a max launch time that the button can be held down for
                //thats the max launch velocity
                //anything less is a fraction of the max launch velocity
                velocity.Y = JUMP_LAUNCH_VEL * (1.0f 
                    - (float)Math.Pow(jumpButtonTime / MAX_JUMP_TIME, JUMP_CONTROL_POWER));
            }
            else
            {
                jumpLaunchInProgress = false;
                jumpButtonTime = 0;
            }

        }
        public Vector2 GetPosition()
        {
            return positon;
        }
        private void KillPlayer()
        {
            ourLevel.SetupLevel();
            positon = Vector2.Zero;
            prevPosition = Vector2.Zero;
            velocity = Vector2.Zero;
            jumpButtonTime = 0;
            jumpLaunchInProgress = false;
        }
    }
}
