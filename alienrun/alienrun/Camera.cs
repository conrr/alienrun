﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace alienrun
{
    class Camera
    {
        //-----------------------------------
        //DATA
        //-----------------------------------
        private Vector2 viewSize;
        private Vector2 Position;
        Player target;

        //-----------------------------------
        //BEHAVIOUR
        //-----------------------------------
        public Camera (Player newTarget, Vector2 newViewSize)
        {
            target = newTarget;
            viewSize = newViewSize;
        }
        //-----------------------------------
        public void Update()
        {
            Position.X = target.GetPosition().X - 0.5f * viewSize.X;
        }
        public void Begin(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(transformMatrix: Matrix.CreateTranslation(-Position.X, -Position.Y, 0));
        }
    }
}
